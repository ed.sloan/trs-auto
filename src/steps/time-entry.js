const fs = require("fs");
const path = require("path");

const { checkIfHoursEnteredIsLessThanRequired } = require("./navigate");

const holidays = fs.readFileSync(path.join(__dirname, "../../holidays.json"), "utf8");
const inputDays = ["Mon", "Tue", "Wed", "Thu", "Fri"];

const addNewTaskRowToTimeEntryTable = async (page, taskCode, comments) => {
  await page.select("#fEnterTime\\:mSelectTask", taskCode);

  await page.$eval("#fEnterTime\\:itNewComments", (el, value) => (el.value = value), comments);

  await page.click("#fEnterTime\\:bCreateTask");

  await page.waitForSelector("#fEnterTime\\:bCommit", { visible: true });
};

const loopThoughDaysOfWeekAndAddTime = async (page, startOfWeek, day, i) => {
  const firstDayInWeek = new Date(startOfWeek);
  const activeDayInTheWeek = new Date(firstDayInWeek.setDate(firstDayInWeek.getDate() + i));

  const listOfHolidays = JSON.parse(holidays);
  const hasHolidayForToday = listOfHolidays.find(
    (holiday) => holiday === activeDayInTheWeek.toISOString().split("T")[0]
  );

  if (hasHolidayForToday) {
    if ((await page.$(`#fEnterTime\\:dtTimeDetails_1\\:itHours${day}`)) === null)
      await addNewTaskRowToTimeEntryTable(page, "Annual Leave", "holiday");

    await page.$eval(`#fEnterTime\\:dtTimeDetails_1\\:itHours${day}`, (el, value) => (el.value = value), "8");
  } else {
    await page.$eval(`#fEnterTime\\:dtTimeDetails_0\\:itHours${day}`, (el, value) => (el.value = value), "8");
  }
};

const fillInTimeDetailsForThisWeek = async (page, startOfWeek) => {
  if ((await page.$("#fEnterTime\\:dtTimeDetails_0\\:otTaskCode")) === null)
    await addNewTaskRowToTimeEntryTable(page, process.env.TASK_CODE, process.env.COMMENTS);

  for await (const [i, day] of inputDays.entries()) {
    await loopThoughDaysOfWeekAndAddTime(page, startOfWeek, day, i);
  }

  await page.click("#fEnterTime\\:bCommit");

  await new Promise((r) => setTimeout(r, 1000));

  await checkIfTheNextWeekIsNeedsTimeEntering(page);
};

const checkIfTheNextWeekIsNeedsTimeEntering = async (page) => {
  await page.waitForSelector("#fEnterTime\\:bNextWeek", { visible: true });

  await page.click("#fEnterTime\\:bNextWeek");

  await page.waitForSelector("#fEnterTime\\:bNextWeek", { visible: true });

  const today = new Date();
  const currentWeek = await page.$eval("#fEnterTime\\:otCurrentWeekDate", (el) =>
    el.innerText.match(/(?<=starting )(.*)(?=\))/g)[0].split("/")
  );
  const currentWeekAsDate = new Date(currentWeek[2], currentWeek[1] - 1, currentWeek[0]);

  const entryTableIsEmpty = (await page.$("#fEnterTime\\:dtTimeDetails\\:totHoursTot")) === null;
  const hoursEnteredIsLessThanRequired = await checkIfHoursEnteredIsLessThanRequired(page);

  if ((entryTableIsEmpty || hoursEnteredIsLessThanRequired) && currentWeekAsDate < today) {
    console.log(`==> Filling in time for the week starting - ${currentWeek.join("/")} ✍`);
    await fillInTimeDetailsForThisWeek(page, currentWeekAsDate);
  } else {
    return console.log("==> Done - all up to date! 🔥");
  }
};

module.exports = {
  checkIfTheNextWeekIsNeedsTimeEntering,
};
