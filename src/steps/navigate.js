const checkIfHoursEnteredIsLessThanRequired = async (page) => {
  if ((await page.$("#fEnterTime\\:dtTimeDetails\\:totHoursTot")) === null) return false;

  const totalEnteredTime = await page.$eval("#fEnterTime\\:dtTimeDetails\\:totHoursTot", (el) => el.innerText);

  if (parseFloat(totalEnteredTime) < parseInt(process.env.REQUIRED_HOURS)) return true;
  return false;
};

const checkIfWeekAlreadyHasFilledInTime = async (page, isFound) => {
  await page.waitForSelector("#fEnterTime\\:bPrevWeek", { visible: true });
  const entryTableIsEmpty = (await page.$("#fEnterTime\\:dtTimeDetails\\:totHoursTot")) === null;
  const hoursEnteredIsLessThanRequired = await checkIfHoursEnteredIsLessThanRequired(page);

  if (entryTableIsEmpty || hoursEnteredIsLessThanRequired) return navigateToTheLastFilledInWeek(page);
  return isFound();
};

const navigateToTheLastFilledInWeek = async (page) => {
  await page.waitForSelector("#fEnterTime\\:bPrevWeek", { visible: true });

  await page.click("#fEnterTime\\:bPrevWeek");

  await checkIfWeekAlreadyHasFilledInTime(page, () => {
    return;
  });
};

module.exports = {
  checkIfHoursEnteredIsLessThanRequired,
  navigateToTheLastFilledInWeek,
};
