const puppeteer = require("puppeteer");
require("dotenv").config();

const { navigateToTheLastFilledInWeek } = require("./steps/navigate");
const { checkIfTheNextWeekIsNeedsTimeEntering } = require("./steps/time-entry");

(async () => {
  const headless = process.env.HEADLESS_MODE === "true";
  const browser = await puppeteer.launch({ headless });
  const page = await browser.newPage();

  try {
    await page.goto("http://10.14.36.194/trs/faces/login.jspx;", { timeout: 7500 });
  } catch (e) {
    console.error("==> ERROR: You need to be connected to the VPN! ⚠️");
    process.exit(1);
  }

  await page.waitForSelector("#welcomeForm\\:login", { visible: true });

  await page.$eval("#welcomeForm\\:username", (el, value) => (el.value = value), process.env.USERNAME);

  await page.$eval("#welcomeForm\\:password", (el, value) => (el.value = value), process.env.PASSWORD);

  await page.click("#welcomeForm\\:login");

  console.log("==> Logged in ✅");
  await page.waitForSelector(".ThemeOfficeMainItem", { visible: true });

  await page.goto("http://10.14.36.194/trs/faces/entertime.jspx");

  console.log("==> Navigating to the last filled in week 🌍");
  await navigateToTheLastFilledInWeek(page);

  console.log("==> Checking if time needs to be added 🔎");
  await checkIfTheNextWeekIsNeedsTimeEntering(page);

  process.exit();
})();
