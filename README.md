# trs-auto

1. Duplicate and rename `.env.example` and `holidays.example.json` to `.env` and `holidays.json`
2. Change the values in each. (the task code is first bit of text to the left of the hyphen taken from the TRS task dropdown e.g. Agile Dev - Build)
3. Run `npm start`
